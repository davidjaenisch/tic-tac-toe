package com.example.TicTacToe.game;

import android.graphics.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: David
 * Date: 12/21/12
 * Time: 2:51 PM
 * Tic Tac Toe! This is the game itself.
 * This is the model so if everything else were removed, this would still work.
 */
public class Game implements GameDisplay {

    public static final int GRID_HEIGHT = 3;

    // For something more complex, I would make a grid object. For a 3x3 grid, this will suffice.
    private Type[][] grid = new Type[GRID_HEIGHT][GRID_HEIGHT];

    // The model sends information to the game displays through a generic interface.
    // The model extends the interface to take advantage of its functions (when called, sends information to displays).
    private List<GameDisplay> gameDisplays = new ArrayList<GameDisplay>();

    private boolean gameOver;

    //The game starts with X's.
    private Type currentMove = Type.X;


    public void playMove(Point p, Type type) throws IllegalMoveException {

        //The game does not allow for out of turn moves, moves after the game is over or moves outside of the grid.
        if (type != currentMove || gameOver || p.x < 0 || p.x >= GRID_HEIGHT || p.y < 0 || p.y >= GRID_HEIGHT) {
            throw new IllegalMoveException();
        }

        //If a piece is already present, the move is not legal either.
        if (grid[p.x][p.y] != null) {
            throw new IllegalMoveException();
        }

        grid[p.x][p.y] = type;
        displayMove(p, type);
        determineWinner();
        if(!gameOver){
            nextTurn(Type.getOtherType(type));
        }
    }

    private void determineWinner() {

        //Check for winners.
        for (int i = 0; i < GRID_HEIGHT; i++) {
            determineWinnerLine(new Point(0, i), new Point(1, i), new Point(2, i));
            determineWinnerLine(new Point(i, 0), new Point(i, 1), new Point(i, 2));
        }
        determineWinnerLine(new Point(0, 0), new Point(1, 1), new Point(2, 2));
        determineWinnerLine(new Point(0, 2), new Point(1, 1), new Point(2, 0));

        //If no one has won, check for a tie.
        if(!gameOver){
            if(checkArrayFull()){
                displayWinner(null);
            }
        }


    }

    // Whether or not the game has ended in a cat's game.
    private boolean checkArrayFull() {
        for(Type[] row: grid){
            for(Type type: row){
                if (type == null) return false;
            }
        }
        return true;
    }

    // Display winner if this line won the game.
    private void determineWinnerLine(Point ... points) {
        Type[] types = new Type[points.length];
        for(int i = 0; i < points.length; i++){
            types[i] = grid[points[i].x][points[i].y];
        }
        if (Type.same(types)) {
            displayWinner(points);
        }
    }

    // Inform displays that a move has been made.
    @Override
    public void displayMove(Point p, Type type) {
        for(GameDisplay gameDisplay : gameDisplays){
            gameDisplay.displayMove(p, type);
        }
    }

    /**
     * Inform displays the winning game moves. Can be sent multiple times to display mult-line victory.
     * @param points
     */
    @Override
    public void displayWinner(Point... points) {
        gameOver = true;
        for(GameDisplay gameDisplay : gameDisplays){
            gameDisplay.displayWinner(points);
        }
    }

    //Iterate to the next turn.
    @Override
    public void nextTurn(Type type){
        currentMove = type;
        for(GameDisplay gameDisplay : gameDisplays){
            gameDisplay.nextTurn(type);
        }
    }

    public void addGameDisplay(GameDisplay gameDisplay){
        gameDisplays.add(gameDisplay);
    }

    public void removeGameDisplay(GameDisplay gameDisplay){
        gameDisplays.remove(gameDisplay);
    }

}
