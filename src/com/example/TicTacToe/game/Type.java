package com.example.TicTacToe.game;

/**
 * Created with IntelliJ IDEA.
 * User: David
 * Date: 12/21/12
 * Time: 3:49 PM
 * The basic types of moves in Tic Tac Toe.
 * Empty is null in this case.
 */
public enum Type {
    X(true),
    O(false);

    private boolean isX;

    private Type(boolean isX){
        this.isX = isX;
    }

    public boolean isX(){
        return isX;
    }

    public static Type getOtherType(Type type){
        if(type == Type.X){
            return Type.O;
        } else if(type == Type.O){
            return Type.X;
        }
        return null;
    }

    public static boolean same(Type ... types){
        if(types.length == 0){
            return false;
        }
        Type test = types[0];
        for(Type type : types){
            if(type == null || type != test){
                return false;
            }
        }
        return true;
    }
    
}
