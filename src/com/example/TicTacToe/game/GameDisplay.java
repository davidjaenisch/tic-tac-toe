package com.example.TicTacToe.game;

import android.graphics.Point;

/**
 * Created with IntelliJ IDEA.
 * User: David
 * Date: 12/21/12
 * Time: 4:22 PM
 * An interface for passing information about the game.
 */
public interface GameDisplay {
    public void displayMove(Point p, Type type);

    public void displayWinner(Point ... points);

    public void nextTurn(Type type);
}
