package com.example.TicTacToe.game;

/**
 * Created with IntelliJ IDEA.
 * User: David
 * Date: 12/22/12
 * Time: 1:45 PM
 * This exception happens whenever a player (or computer) inputs an incorrect move. Currently, we just print to stack trace and move on.
 * This may be relevant for the AI.
 */
public class IllegalMoveException extends Exception {
}
