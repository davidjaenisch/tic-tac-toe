package com.example.TicTacToe;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.example.TicTacToe.ai.Computer;
import com.example.TicTacToe.display.GameView;
import com.example.TicTacToe.game.Game;
import com.example.TicTacToe.game.Type;

//The Activity class. Android's version of main.
public class TicTacToe extends Activity {

    private Game game;

    private GameView gameView;

    private Computer computer;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Load the main menu.
        restart();
    }

    ///Used to return to main menu and reset the game.
    public void restart(){
        setContentView(R.layout.main);
    }

    //chooseHuman and chooseComputer are both referenced in main.xml
    //When you set a text value for onClick in the xml file, onClick listeners are generated in the Activity
    // and refer to these functions (with the name specified in the string and a single view as the parameter).
    public void chooseHuman(View v) {
        startGame();
    }

    //Beep boop.
    public void chooseComputer(View v) {
        startGame();
        computer = new Computer(game, (Math.random() > 0.5) ? Type.O : Type.X);

    }

    //Called to start the game.
    private void startGame() {

        //Start the Game and initialize the Game View.
        game = new Game();
        //Get the screen width and pass it to the Game View.
        gameView = new GameView(this, game, getWindowManager().getDefaultDisplay().getWidth());
        game.addGameDisplay(gameView);

        //Layout inflation is a process where you take xml code and turn it into java.
        //We'll do xml unmarshalling at some point. It's a good way to save data between games.
        LayoutInflater inflater = LayoutInflater.from(this);
        //We're passing it the location of the game xml file. It just contains a linear layout with a textfield.
        LinearLayout gameLayout = (LinearLayout) inflater.inflate(R.layout.game2, null);

        //Adding the meat and potatoes to our linear layout.
        gameLayout.addView(gameView);

        //Loading the linear layout.
        setContentView(gameLayout);
    }
}
