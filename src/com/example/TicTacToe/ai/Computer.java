package com.example.TicTacToe.ai;

import android.graphics.Point;
import com.example.TicTacToe.game.Game;
import com.example.TicTacToe.game.GameDisplay;
import com.example.TicTacToe.game.IllegalMoveException;
import com.example.TicTacToe.game.Type;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: David
 * Date: 12/21/12
 * Time: 5:06 PM
 * AI for single player games. Plays random moves.
 */
public class Computer implements GameDisplay {

    private Type type;

    private Game game;

    //The computer simulates a grid.
    private Type[][] grid = new Type[Game.GRID_HEIGHT][Game.GRID_HEIGHT];

    public Computer(Game game, Type type){

        this.game = game;
        this.type = type;

        game.addGameDisplay(this);

        if(type == Type.X){
            nextTurn(Type.X);
        }
    }

    public Point getNextMove() {
        ArrayList<Point> points = new ArrayList<Point>();
        for (int i = 0; i < Game.GRID_HEIGHT; i++) {
            for (int j = 0; j < Game.GRID_HEIGHT; j++) {
                if(grid[i][j] == null){
                    points.add(new Point(i, j));
                }
            }
        }
        if(points.size() == 0){
            return null;
        }
        int rand = (int) (points.size() * Math.random());
        return points.get(rand);
    }

    public Type getType() {
        return type;
    }

    @Override
    public void displayMove(Point p, Type type) {
        grid[p.x][p.y] = type;

    }

    @Override
    public void displayWinner(Point... points) {
        //The computer does not need to display the winner.
    }

    @Override
    public void nextTurn(Type type) {

        if(this.type == type){
            try {
                Point move = getNextMove();
                if(move != null){
                    game.playMove(move, this.type);
                }
            } catch (IllegalMoveException e) {
                //TODO: Make computer understand failure (also love).
            }
        }
    }
}
