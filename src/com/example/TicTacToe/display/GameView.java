package com.example.TicTacToe.display;

import android.content.Context;
import android.graphics.Point;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.example.TicTacToe.R.drawable;
import com.example.TicTacToe.TicTacToe;
import com.example.TicTacToe.game.Game;
import com.example.TicTacToe.game.GameDisplay;
import com.example.TicTacToe.game.IllegalMoveException;
import com.example.TicTacToe.game.Type;

/**
 * Created with IntelliJ IDEA.
 * User: David
 * Date: 12/21/12
 * Time: 5:23 PM
 * The GameView is the custom code which is the front end of the game. Games don't fit well into .xml files but they can be added as Views to Layouts.
 */
public class GameView extends RelativeLayout implements GameDisplay {

    //Reference to game.
    private Game game;

    private ImageView background;

    private TypeView[][] grid = new TypeView[Game.GRID_HEIGHT][Game.GRID_HEIGHT];

    private boolean gameOver;

    private Type currentMove = Type.X;

    /**
     *
     * @param context Context of the view.
     * @param game The model for the gameView.
     * @param layoutWidth The screen width. This way we can adjust the size easily. There doesn't seem to be a good way of doing this in xml.
     */
    public GameView(Context context, Game game, int layoutWidth) {
        super(context);

        this.game = game;

        //Loading in the background.
        background = new ImageView(context);
        background.setLayoutParams(new LayoutParams(layoutWidth, layoutWidth));
        background.setImageResource(drawable.grid);

        this.addView(background);
        for (int i = 0; i < Game.GRID_HEIGHT; i++) {
            for (int j = 0; j < Game.GRID_HEIGHT; j++) {
                addSpace(context, i, j, layoutWidth);
            }
        }
    }

    //Adds an image view to show an X or an O.
    private void addSpace(Context context, final int x, final int y, int layoutWidth) {
        TypeView view = new TypeView(context);

        //Percentages of the width. May result in strange behavior if layout width is not divisible by 100 pixels.
        //Do not set larger than the screen.
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (0.3 * layoutWidth),(int) (0.3 * layoutWidth));
        layoutParams.setMargins((int) ((0.02 + (0.34 * x)) * layoutWidth), (int) ((0.34 * y) * layoutWidth), 0, 0);
        view.setLayoutParams(layoutParams);
        view.setClickable(true);

        //This onClick listener allows the player to make moves.
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                playMove(x, y);
            }
        });

        //Adding to the layout and grid of image views.
        this.addView(view);
        grid[x][y] = view;
    }

    //Executes the onClick function. Either plays a move or restarts the game if it has ended.
    private void playMove(int x, int y) {
        if(!gameOver){
            try {
                game.playMove(new Point(x,y), currentMove);
            } catch (IllegalMoveException e) {
                e.printStackTrace();
            }
        } else {
            ((TicTacToe) getContext()).restart();
        }

    }

    //Displays the X or O move.
    @Override
    public void displayMove(Point p, Type type) {
        grid[p.x][p.y].setType(type);
    }

    //Changes the winning lines to be red.
    @Override
    public void displayWinner(Point... points) {
        this.gameOver = true;
        if(points != null){
            for (Point p : points) {
                grid[p.x][p.y].win();
            }
        }
    }

    @Override
    public void nextTurn(Type type) {
        this.currentMove = type;
    }
}
