package com.example.TicTacToe.display;

import android.content.Context;
import android.widget.ImageView;
import com.example.TicTacToe.R.drawable;
import com.example.TicTacToe.game.Type;

/**
 * Created with IntelliJ IDEA.
 * User: David
 * Date: 12/22/12
 * Time: 10:58 AM
 * This is an ImageView extended to make its main use cases easy (setType and win).
 */
public class TypeView extends ImageView {

    //Type X or O
    private Type type;

    public TypeView(Context context) {
        super(context);
    }



    public void setType(Type type){
        this.type = type;
        if(type == Type.O){
            setImageResource(drawable.o);
        } else if(type == Type.X){
            setImageResource(drawable.x);
        } else {
            setImageResource(0);
        }

    }

    public void win() {
        if(this.type == Type.O){
            setImageResource(drawable.red_o);
        } else if(this.type == Type.X){
            setImageResource(drawable.red_x);
        } else {
            setImageResource(0);
        }
    }
}
